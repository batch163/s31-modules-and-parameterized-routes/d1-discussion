const express = require('express');
const router = express.Router();

const {
	createTask,
	getAllTasks,
	deleteTask,
	updateTask,
	getSpecificTask,
	findOneTask,
	completeTask

} = require('./../controllers/taskControllers')

//CREATE A TASK
router.post('/', async (req, res) => {
	// console.log(req.body)	{"name": "watching"}	//object

	try{
		await createTask(req.body).then(result => res.send(result))

	} catch(err){
		res.status(400).json(err.message)

	} 
})
 


//GET ALL TASKS
router.get('/', async (req, res) => {
	const qId = req.query.id
	const qStatus = req.query.status

	console.log(qId)
	console.log(qStatus)

	await getAllTasks(qId, qStatus).then(result => res.send(result))

	// try{
	// 	await getAllTasks().then(result => res.send(result))

	// } catch(err){
	// 	res.status(500).json(err)
	// }
})



//DELETE A TASK

router.delete('/:id/delete', async (req, res) => {
	// console.log(typeof req.params)	//object
	//console.log(typeof req.params.id)	//string

	try{
		await deleteTask(req.params.id).then(response => res.send(response))

	}catch(err){
		res.status(500).json(err)
	}
})



//UPDATE A TASK
router.put('/', async (req, res) => {
	// console.log(req.params.taskId)
	const id = req.params.taskId
	// console.log(req.body)

	// await updateTask(id, req.body).then(response => res.send(response))

	await updateTask(req.body.name, req.body).then(response => res.send(response))
})


// ACTIVITY

// Create a route for getting a specific task.
// Create a controller function for retrieving a specific task.
// Return the "result" back to the client/Postman.	//single document
// Process a GET request at the /tasks/:id route using postman to get a specific task.

//GET A SPECIFIC TASK
router.get('/:id', async (req, res) => {
	//console.log(req.params)	//{ id: '6226d2aa96fba6a59fcbc4e1' } //object

	// await getSpecificTask(req.params.id).then(result => res.send(result))


	try{
		await getSpecificTask(req.params.id).then(result => res.send(result))

	} catch(err){
		res.status(500).json(err)
	}
})

/*
// findOne() option in get method
router.get('/findOne', (req, res) => {
	//console.log(req.body)	//{ name: 'Coding' }	//object

	findOneTask(req.body.name).then(result => res.send(result))
})

*/



// Create a route for changing the status of a task to complete.
// Create a controller function for changing the status of a task to complete.
// Return the result back to the client/Postman.
// Process a PUT request at the /tasks/:id/complete route using postman to update a task.


//UPDATE A SPECIFIC TASK TO CHANGE STATUS TO COMPLETE
router.put('/:id/complete', async (req, res) => {

	try{
		await completeTask(req.params.id).then(result => res.send(result))

	} catch(err){
		res.status(500).json(err)
	}
})




module.exports = router;