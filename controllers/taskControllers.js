
const Task = require(`./../models/Task`)

// Business Logic for Creating a task
/*
1. Add a functionality to check if there are duplicate tasks
	- If the task already exists in the database, we return a message `Duplicate Task found`
	- If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/
module.exports.createTask = async (reqBody) => {
	/*
		let newTask = new Task({
			name: reqBody.name
		})

		//use save() method to insert the new document in the database
		return await newTask.save().then((savedTaks, err) => {
			// console.log(savedTaks)
			if(savedTaks){
				return savedTaks
			} else {
				return err
			}
		})

	*/

//or

	return await Task.findOne({name: reqBody.name}).then( (result, err) => {
		// console.log(result)	//a document

		// If the task already exists in the database, we return a message `Duplicate Task found`
		if(result != null && result.name == reqBody.name){
			return true

		} else {

			if(result == null){

				let newTask = new Task({
					name: reqBody.name
				})

				return newTask.save().then( result => result )

			}
		}
	})
}



// GET ALL TASKS

// Business Logic for getting all the tasks
/*
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/
module.exports.getAllTasks = async (id, status) => {

	return await Task.find().then( (result, err) => {
		if(result){
			return result
		} else {
			return err
		}
	})

	/*
		if(id && status){
			return await Task.find({_id: id, status: status}).then( (result, err) => {
				if(result){
					return result
				} else {
					return err
				}
			})
		} else if(id){
			return await Task.find({_id: id}).then( (result, err) => {
				if(result){
					return result
				} else {
					return err
				}
			})
		} else if(status){
			return await Task.find({status: status}).then( (result, err) => {
				if(result){
					return result
				} else {
					return err
				}
			})
		} else {
			return await Task.find().then( (result, err) => {
				if(result){
					return result
				} else {
					return err
				}
			})
		}
	*/
}



// DELETE A TASK
module.exports.deleteTask = async (id) => {

	return await Task.findByIdAndDelete(id).then( result => {
		try{
			if(result != null){
				return true
			} else {
				return false
			}

		}catch(err){
			return err
		}
	})
}



// UPDATE A TASK
module.exports.updateTask = async (name, reqBody) => {
	// console.log(reqBody)

	// return await Task.findByIdAndUpdate(id, {$set: reqBody}, {new:true}).then(result => result)

	return await Task.findOneAndUpdate({name: name}, {$set: reqBody}, {new:true})
	.then(response => {
		if(response == null){
			return {message: `no existing document`}
		} else {
			if(response != null){
				return response
			}
		}
	})
}



//GET SPECIFIC TASK

module.exports.getSpecificTask = async (id) => {

	return await Task.findById(id).then( doc => {
		if(doc == null){
			return {message: `No existing document`}

		} else {
			if(doc !== null){
				return doc
			}
		}
	})
}


/*
// findOne() option in get method
module.exports.findOneTask = async (reqBody) => {
	//console.log(reqBody)	//

	return await Task.findOne({name: reqBody}).then( result => {
		if(result){
			return result
		} else {
			if(result == null){
				return {message: `No existing document found!`}
			}
		}
	})
}

*/


module.exports.completeTask = async (id) =>{

	const update = {
		status: "complete"
	}

	return await Task.findByIdAndUpdate(id, {$set: {status: "complete"}}, {new:true}).then(result => {
		if(result){
			return result
		} else{
			if(result == null){
				return {message: `No existing document found!`}
			}
		}
	})
}


